package ru.t1.godyna.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.event.ConsoleEvent;
import ru.t1.godyna.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    private final static String NAME = "task-complete-by-id";

    @NotNull
    private final static String DESCRIPTION = "Complete task by id.";

    @Override
    @EventListener(condition = "@taskCompleteByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        taskEndpointClient.changeTaskStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
